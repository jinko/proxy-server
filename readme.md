```
Usage: php ./index.php [options...]
       shell/runproxyservice [options...]
       shell/runproxyservicebg [options...]      (Run in background)
  -i          The ip address to binding. Default value 0.0.0.0
  -p          The port to listening.
              Default value 1080 for http proxy, 1081 for socks5 proxy
  --http      Run HTTP proxy server
  --socks5    Run Socks5 proxy server
  --help      Help document
```

eg:

```shell
# Start the socks5 proxy service and listen on the default port 1081
php ./index.php

# Start the socks5 proxy service and listen to the specified port 12345
php ./index.php -p12345

# Use a shell script to start the socks5 proxy service and listen to the specified port 12345
shell/runproxyservice -p12345

# Use a shell script to run the socks5 proxy service in the background and listen to the specified port 12345
shell/runproxyservicebg -p12345

# Start the http proxy service and listen on the default port 1080
php ./index.php --http

# Start the http proxy service and listen to the specified port 12346
php ./index.php -p12346

# Use a shell script to start the http proxy service and listen to the specified port 12346
shell/runproxyservice -p12346 --http

# Use a shell script to run the socks5 proxy service in the background and listen to the specified port 12345
shell/runproxyservicebg -p12345  --http

```

Please add permissions to the script file before running.
```shell
sudo chmod ug+x -R ./shell
```


If you need to close the service running in the background, you can first use the `shell/findport` script to find the process listening on the specified port (such as 1080), and then run `kill` command to close the corresponding background program. Here is the example:

```shell
# Find the pid of the process listening on port 1080 or 1081 (default port)
sudo shell/findport

# Print out the information below
# tcp        0      0 *:1081                  *:*                     LISTEN      18882/php

# Kill the process
sudo kill -9 18882


# Find the pid of the process listening on specified port such as 12345
sudo shell/findport 12345

# Print out the information below
# tcp        0      0 *:12345                  *:*                     LISTEN      18891/php

# Kill the process
sudo kill -9 18891

```