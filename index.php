<?php

use Helper\Logger;
use Socks5\Socks5Server;

ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);

require_once "Socks5/Socks5Server.php";
require_once "Http/HttpProxyServer.php";
require_once "Helper/Logger.php";

class Index {
    public function __construct()
    {
        if(!extension_loaded('sockets')) {
            Logger::record('Run fail! php_sockets extension not loaded!');
        }
        
        $options = getopt('p::i::', ['socks5', 'http', 'help']);
        $ip = $options['i'] ?: '0.0.0.0';
        $serverClass = null;
        
        if(isset($options['help'])) {
            $this->help();
            return;
        }
        
        if(isset($options['http'])) {
            $port = $options['p'] ?: 1080;
            $serverClass = HttpProxyServer::class;
        } else {
            $port = $options['p'] ?: 1081;
            $serverClass = Socks5Server::class;
        }
        
        new $serverClass($port, $ip);
    }
    
    public function help() {
        echo <<<help
Usage: php ./index.php [options...]
       shell/runproxyservice [options...]
       shell/runproxyservicebg [options...]      (Run in background)
  -i          The ip address to binding. Default value 0.0.0.0
  -p          The port to listening.
              Default value 1080 for http proxy, 1081 for socks5 proxy
  --http      Run HTTP proxy server
  --socks5    Run Socks5 proxy server
  --help      Help document
  
help;
    }
}

new Index;
