<?php

use Net\Connection;

ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);

require_once "Socks5Server.php";
require_once "Socks5Trait.php";

class Client {
    use Socks5Trait;
    
    public function __construct()
    {
        // $ip = '127.0.0.1';
        // $port = '55555';
        // $ip = '192.168.68.155';
        // $port = '55555';
        $ip = '172.217.24.74';
        $port = '443';
        
        $client = new Connection();
        // $client->setSendTimeout(5);
        // $client->setRecvTimeout(5);
        // $client->setBlock(true);
        $client->setConnectTimeout(3);
        $client->connect($ip, $port);
    
        $client->onConnectFail(function() {
            echo "fail!";
            die;
        })
        ->onConnect(function() {
            echo "ok!";
            die;
        })
        ->onClose(function() {
            echo "close!";
            die;
        });
        
        Connection::dispatch();
        
        //
        // if($client->getLastError()) {
        //     echo "connect fail: {$client->getLastErrorMessage()}\n";
        // } else {
        //     echo "connect success!\n";
        // }
        //
        // $client->setBlock(false);
        //
        // // $client->setBlock();
        // $client->send($this->buildPackage(1, $this->METHOD_ANONYMOUS));
        //
        // $msg = $client->recv();
        // $this->debugPackage($msg);
        //
        // $client->send($this->buildPackage(1,0,1,14,215,177,39,0,80));
        //
        // $msg = $client->recv();
        // $this->debugPackage($msg);
        //
        // // $client->send("");
        // sleep(10);
    }
}

new Client;
