<?php
namespace Net;

class Event {
    
    const PARAM_ERROR_CODE = 'error_code';
    const PARAM_ERROR_MESSAGE = 'error_message';
    const PARAM_ERROR_TRACES = 'error_traces';
    const PARAM_CONNECTION = 'connection';
    const PARAM_SERVER = 'server';
    const PARAM_MESSAGE = 'message';
    const PARAM_ERROR_METHOD = 'error_method';
    const PARAM_ADDRESS = 'address';
    const PARAM_PORT = 'port';
    
    protected $_stopTransmit;
    protected $_preventDefault;
    protected $_type;
    protected $_when;
    protected $_params;
    
    public function __construct($type=null, $when=null, $params=[])
    {
        $this->_type = $type;
        $this->_when = $when;
        $this->_params = $params;
    }
    
    public function isPreventDefault() {
        return $this->_preventDefault;
    }
    
    public function preventDefault() {
        $this->_preventDefault = true;
        return $this;
    }
    
    public function isStopTransmit() {
        return $this->_stopTransmit;
    }
    
    public function stopTransmit() {
        $this->_stopTransmit = true;
        return $this;
    }
    
    public function type() {
        return $this->_type;
    }
    
    public function when() {
        return $this->when();
    }
    
    public function param($key=null) {
        if($key) {
            return $this->_params[$key];
        } else {
            return $this->_params;
        }
    }
    
    public function getMessage() {
        return $this->param(self::PARAM_MESSAGE);
    }
    
    public function getErrorCode() {
        return $this->param(self::PARAM_ERROR_CODE);
    }
    
    public function getErrorTraces() {
        return $this->param(self::PARAM_ERROR_TRACES);
    }
    
    public function getErrorMethod() {
        return $this->param(self::PARAM_ERROR_METHOD);
    }
    
    public function getErrorMessage() {
        return $this->param(self::PARAM_ERROR_MESSAGE);
    }
    
    public function getAddress() {
        return $this->param(self::PARAM_ADDRESS);
    }
    
    public function getPort() {
        return $this->param(self::PARAM_PORT);
    }
    
    /**
     * @return Connection
     */
    public function getConnection() {
        return $this->param(self::PARAM_CONNECTION);
    }
}